package com.love.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@EnableEurekaClient
@SpringBootApplication
public class SpringBootJedisApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringBootJedisApplication.class,args);
	}
}
