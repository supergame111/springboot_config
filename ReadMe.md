[TOC]()

# SpringBoot系列教程

1. [Spring Boot 的配置文件 application.properties](https://blog.csdn.net/An1090239782/article/details/107159072)
2. [SpringBoot-配置Log4j2，实现不同环境日志打印](https://blog.csdn.net/An1090239782/article/details/107159157)
3. [SpringBoot-配置系统全局异常映射处理](https://blog.csdn.net/An1090239782/article/details/107159182)
4. [SpringBoot-定时任务和异步任务的使用方式](https://blog.csdn.net/An1090239782/article/details/107159200)
5. [SpringBoot-多个拦截器配置和使用场景](https://blog.csdn.net/An1090239782/article/details/107159227)
6. [SpringBoot-集成Druid连接池，配置监控界面](https://blog.csdn.net/An1090239782/article/details/107159271)
7. [SpringBoot-集成Redis数据库，实现缓存管理](https://blog.csdn.net/An1090239782/article/details/107159315)
8. [SpringBoot-集成JPA持久层框架，简化数据库操作](https://blog.csdn.net/An1090239782/article/details/107159335)
9. [SpringBoot-整合Mybatis框架，集成分页助手插件](https://blog.csdn.net/An1090239782/article/details/107159383)
10. [SpringBoot-配置AOP切面编程，解决日志记录业务](https://blog.csdn.net/An1090239782/article/details/107159401)
11. []()
12. []()
13. []()


# SpringBoot系列优秀博文
## 博客园
- [BAT的乌托邦](https://www.cnblogs.com/yourbatman/category/1506917.html)
- [大叔杨](https://www.cnblogs.com/yanghj/tag/spring%20boot/)
- [朝雾轻寒](https://www.cnblogs.com/zwqh/tag/Spring%20Boot/)
- [三国梦回](https://www.cnblogs.com/grey-wolf/category/1676533.html)
- []()

# SpringBoot系列优质项目
## SpringBoot学习项目
- [spring-boot-pay](https://gitee.com/52itstyle/spring-boot-pay)
这是一个支付案例，提供了包括支付宝、微信、银联在内的详细支付代码案例，对于有支付需求的小伙伴来说，这个项目再合适不过了。

项目效果图：
![image](https://segmentfault.com/img/remote/1460000021266774)

- [springboot-plus](https://gitee.com/xiandafu/springboot-plus)
一个基于 SpringBoot 2 的管理后台系统,包含了用户管理，组织机构管理，角色管理，功能点管理，菜单管理，权限分配，数据权限分配，代码生成等功能 相比其他开源的后台系统，SpringBoot-Plus 具有一定的复杂度。系统基于 Spring Boot2.1 技术，前端采用了Layui2.4。数据库以 MySQL/Oracle/Postgres/SQLServer 为实例，理论上是跨数据库平台。

项目效果图：
![image](https://segmentfault.com/img/remote/1460000021266778)

- [spring-boot-seckill](https://gitee.com/52itstyle/spring-boot-seckill)

从 0 到 1 构建分布式秒杀系统，脱离案例讲架构都是耍流氓，SpringBoot 开发案例从 0 到 1 构建分布式秒杀系统，项目案例基本成型，逐步完善中。
![image](https://segmentfault.com/img/remote/1460000021266780)

- [litemall](https://github.com/linlinjava/litemall)

一个商城项目，包括 Spring Boot 后端 + Vue 管理员前端 + 微信小程序用户前端 + Vue用户移动端，功能包括、分类列表、分类详情、品牌列表、品牌详情、新品首发、人气推荐、优惠券列表、优惠券选择、团购（团购业务有待完善）、搜索、商品详情、商品评价、商品分享、购物车、下单、订单列表、订单详情、地址、收藏、足迹、意见反馈以及客服；管理平台功能包括会员管理、商城管理、商品管理、推广管理、系统管理、配置管理、统计报表等。

项目效果图：
![image](https://segmentfault.com/img/remote/1460000021266782)

- [jeeSpringCloud](https://gitee.com/JeeHuangBingGui/jeeSpringCloud)

基于 SpringBoot2.0 的后台权限管理系统界面简洁美观敏捷开发系统架构。核心技术采用Spring、MyBatis、Shiro 没有任何其它重度依赖。 互联网云快速开发框架,微服务分布式代码生成的敏捷开发系统架构。项目代码简洁,注释丰富,上手容易,还同时集中分布式、微服务,同时包含许多基础模块和监控、服务模块。模块包括:定时任务调度、服务器监控、平台监控、平台设置、开发平台、单点登录、Redis 分布式高速缓存、会员、营销、在线用户、日志、在线人数、访问次数、调用次数、直接集群、接口文档、生成模块、代码实例、安装视频、教程文档 代码生成(单表、主附表、树表、列表和表单、redis 高速缓存对接代码、图表统计、地图统计、vue.js )、dubbo、springCloud、SpringBoot、mybatis、spring、springmvc。
![image](https://segmentfault.com/img/remote/1460000021266781)

## SpringBoot前后端分离项目
- [美人鱼](https://gitee.com/mumu-osc/NiceFish)
NiceFish（美人鱼） 是一个系列项目，目标是示范前后端分离的开发模式:前端浏览器、移动端、Electron 环境中的各种开发模式；后端有两个版本：SpringBoot 版本和 SpringCloud 版本，前端有 Angular 、React 以及 Electron 等版本。

- [微人事](https://github.com/lenve/vhr)
微人事是一个前后端分离的人力资源管理系统，项目采用 SpringBoot + Vue 开发。项目打通了前后端，并且提供了非常详尽的文档，从 Spring Boot 接口设计到前端 Vue 的开发思路，作者全部都记录在项目的 wiki 中，是不可多得的 Java 全栈学习资料。

![image](https://segmentfault.com/img/remote/1460000020416007)
![image](https://segmentfault.com/img/remote/1460000020416008)

- [bootshiro](https://gitee.com/tomsun28/bootshiro)
bootshiro 是基于 Spring Boot + Shiro + JWT 的真正 RESTful URL 资源无状态认证权限管理系统的后端,前端 usthe 。区别于一般项目，该项目提供页面可配置式的、动态的 RESTful api 安全管理支持，并且实现数据传输动态秘钥加密，jwt 过期刷新，用户操作监控等，加固应用安全。

项目效果图：
![image](https://segmentfault.com/img/remote/1460000020416009)

- [open-capacity-platform](https://gitee.com/owenwangwen/open-capacity-platform)
open-capacity-platform 微服务能力开放平台，简称 ocp ，是基于 layui + springcloud 的企业级微服务框架(用户权限管理，配置中心管理，应用管理，....)，其核心的设计目标是分离前后端，快速开发部署，学习简单，功能强大，提供快速接入核心接口能力，其目标是帮助企业搭建一套类似百度能力开放平台的框架。

项目效果图：
![image](https://segmentfault.com/img/remote/1460000020416010)

- [V部落](https://github.com/lenve/VBlog)

V部落是一个多用户博客管理平台，采用 Vue + SpringBoot + ElementUI 开发。这个项目最大的优势是简单，属于功能完整但是又非常简单的那种，非常非常适合初学者。

项目效果图：
![image](https://segmentfault.com/img/remote/1460000020416011)

- [悟空 CRM](https://gitee.com/wukongcrm/72crm-java)
悟空 CRM 是基于 jfinal + vue + ElementUI 的前后端分离 CRM 系统。

老实说，jfinal 了解下就行了，没必要认真研究，Vue + ElementUI 的组合可以认真学习下、前后端交互的方式可以认真学习下。
![image](https://segmentfault.com/img/remote/1460000020416012)

- [paascloud-master](https://github.com/paascloud/paascloud-master)
paascloud-master 核心技术为 SpringCloud + Vue 两个全家桶实现，采取了取自开源用于开源的目标，所以能用开源绝不用收费框架，整体技术栈只有阿里云短信服务是收费的，都是目前 java 前瞻性的框架，可以为中小企业解决微服务架构难题，可以帮助企业快速建站。由于服务器成本较高，尽量降低开发成本的原则，本项目由 10 个后端项目和 3 个前端项目共同组成。真正实现了基于 RBAC、jwt 和 oauth2 的无状态统一权限认证的解决方案，实现了异常和日志的统一管理，实现了 MQ 落地保证 100% 到达的解决方案。

项目效果图：
![image](https://segmentfault.com/img/remote/1460000020416013)


# SpringBoot资源
- [SpringBoot教程汇总](http://www.springboot.wiki/)
- [SpringBoot中文索引](http://springboot.fun/)
- [C语言中文网-SpringBoot教程](http://c.biancheng.net/spring_boot/)
- [江南一点雨](https://segmentfault.com/a/1190000020148808)
  - [SpringBoot系列博文](https://www.javaboy.org/springboot/)
