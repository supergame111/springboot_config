package com.love.springboot;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@NacosPropertySource(dataId = "springboot-nacos", autoRefreshed = true)
@RestController
public class SpringBootNacos {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootNacos.class, args);
    }

    @NacosValue(value = "${love.author:}", autoRefreshed = true)
    private String zlkjAuthor;

    @NacosValue(value = "${love.title:}", autoRefreshed = true)
    private String zlkjTitle;

    @RequestMapping("/getConfig")
    public String getConfig() {
        System.out.println("Author:" + zlkjAuthor + ";Title:" + zlkjTitle);
        return zlkjAuthor + "===" + zlkjTitle;
    }
}