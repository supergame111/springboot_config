package com.love.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author aishiyushijiepingxing
 * @description
 * @date 2020/5/18
 */
@SpringBootApplication
public class SpringBootExceptionApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootExceptionApplication.class, args);
    }
}
