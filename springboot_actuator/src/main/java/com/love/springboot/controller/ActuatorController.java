package com.love.springboot.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author aishiyushijiepingxing
 * @description
 * @date 2020/5/18
 */
@RestController
public class ActuatorController {

    @RequestMapping("/actuator")
    public String actuator() {
        return "actuator";
    }
}
