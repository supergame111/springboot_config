package com.love.springboot.service.impl;

import com.love.springboot.service.AccountService;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class AccountServiceImpl implements AccountService {

    @Resource
    private JdbcTemplate jdbcTemplate;

    @Override
    //所有的覆写方法，必须加@Override注解。
    //反例：getObject()与get0Bject的问题，一个是字母的O，一个是数字的0，加@Override可以准确判断是否覆盖成功。
    //另外，如果在抽象类中队方法签名进行修改，其实现类会马上编译报错。
    public void out(String outer, Integer money) {
        String sql = "update account set money = money - ? where username = ?";
        jdbcTemplate.update(sql, money, outer);
    }

    @Override
    public void in(String inner, Integer money) {
        String sql = "update account set money = money + ? where username = ?";
        jdbcTemplate.update(sql, money, inner);
    }

}
